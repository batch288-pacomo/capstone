const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
		userAccount: {
			type: String,
			required: [true, "user account is registered email!"]
		},
		purchaseOn: {
			type: Date,
			default: new Date()
		},
		orders: [{
			productId: {
				type: String,
				required: [true, "product ID is required!"]
			},
			productName: {
				type: String,
				required: [true, "product Name is required!"]
			},
			description: {
				type: String,
				required: [true, "product description is required!"]
			},
			quantity: {
				type: Number,
				required: [true, "product quantity is required!"]
			},
			price: {
				type: Number,
				required: [true, "product price is required!"]
			},
			total: {
				type: Number,
				required: [true, "total is required!"]
			}
		}]
		
})

const Orders = mongoose.model("Order", orderSchema);

module.exports = Orders;