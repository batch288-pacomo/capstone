const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
		productId: {
			type: String,
			required: [true, "product ID is required!"]
		},
		productName: {
			type: String,
			required: [true, "product name is required!"]
		},
		description: {
			type: String,
			required: [true, "product description is required!"]
		},
		price: {
			type: Number,
			required: [true, "product price is required!"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}

})

const Products = mongoose.model("Product", productSchema);

module.exports = Products;