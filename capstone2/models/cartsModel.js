const mongoose = require("mongoose");
const cartSchema = new mongoose.Schema({

		userAccount: {
			type: String,
			required: [true, "user account is registered email!"]
		},
		orders: [{
				productId: String,
				productName: String,
				quantity: {
					type: Number,
					required: true,
					min:[1, "Add atleast 1 quantity"],
					default:1
				},
				price: Number,
				subtotal: {
					type: Number,
					required: true,
					default: 0
				}
		}],
		total: {
			type: Number,
			required: true,
			default: 0
		}

})

const Carts = mongoose.model("Cart", cartSchema);

module.exports = Carts;