const Orders = require("../models/ordersModel.js");
const auth = require("../auth.js");
const Products = require("../models/productsModel.js")
// Create Order(non-admin,token)


module.exports.createOrder = async (request, response) => {
const userData = auth.decode(request.headers.authorization);
let productId = request.body.productId;
let quantity = request.body.quantity;


Products.findOne({productId: productId})
.then(result => {

	if(!userData.isAdmin&&result.isActive){
		let newOrder = new Orders({
			userAccount: userData.email,
			purchaseOn: new Date(),
			orders: [{
				productId: productId,
				productName: result.productName,
				description: result.description,
				quantity: quantity,
				price: result.price,
				total: quantity*result.price
			}]		
	});
		newOrder.save()
		.then(saved => response.send(`Order successfully made!
			${newOrder.orders[0]}
			`))
		.catch(error => response.send(error));
	}else if(!result.isActive){
		return response.send("product is unavailable")
	}else{
		return response.send("non-admin access only")
	}

})
.catch(error => response.send(error));
	

}


// Check my Order(non-admin,token)
module.exports.myOrder = async (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(!userData.isAdmin){
		Orders.find({userAccount: userData.email})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}else{
		return response.send("non-admin access only")
	}

}


// Retrieve all users order
module.exports.allUsersOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
		Orders.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}else{
		return response.send(`You don't have access in this route!`)
	}
}

// Testing/////////////////////////////////////

