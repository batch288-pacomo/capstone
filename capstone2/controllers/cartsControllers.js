const Carts = require("../models/cartsModel.js");
const Products = require("../models/productsModel.js");
const auth = require("../auth.js");

// check cart(login User)
module.exports.getProduct = async (request, response) => {
	const userData = auth.decode(request.headers.authorization)
try{

	if(!userData.isAdmin){

	let cart = await Carts.findOne({userAccount: userData.email})
		if(cart && cart.orders.length>0){
			return response.send(cart);
		}else{
			return response.send(`acount: ${userData.email}
				no item in the cart`);
		}

	}else{
		return response.send(`You don't have access in this route!`)
	}

}
catch(error){return response.send(`CatchError: ${error}`)}
}

// add to cart(login User)
module.exports.addtoCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.body.productId;
	const quantity = request.body.quantity;
try{

	if(!userData.isAdmin){
	
	let cart = await Carts.findOne({userAccount: userData.email});
	let product = await Products.findOne({productId: productId});
	if(!product){
		return response.send("no product found!")
	}
	const price = product.price;
	const productName = product.productName;
	const subtotal = product.price;
	const isActive = product.isActive;
	
	//check is the product is active/archive
if(isActive){
	if(cart){
		// if cart exists for the user
		let itemIndex = cart.orders.findIndex(product => product.productId == productId);
		

		// Check if product exist or not
		if(itemIndex > -1){
			
			let productItem = cart.orders[itemIndex];
			
			productItem.quantity += quantity;
			productItem.subtotal = price*quantity;
			cart.orders[itemIndex] = productItem;
			
		}else{
			cart.orders.push({productId, productName, quantity, price, subtotal});
		}
		cart.total += quantity*price;
		cart = await cart.save();
		return response.send(cart);
	}else{
		// no cart exists, create one
		const newCart = await Carts.create({
			userAccount: userData.email,
			orders:[{productId, productName, quantity, price, subtotal}],
			total: quantity*price
		});
		return response.send(newCart);
	}

}else{
	return response.send(`${productName} is unavailable at the moment`)}
	
	}else{
		return response.send(`You don't have access in this route!`)
		}

}
catch(error){return response.send(`CatchError: ${error}`)}

	
	}

// delete to cart(login User)
module.exports.removetoCart =  async (request,response) => {
	const userData = auth.decode(request.headers.authorization)
    const productId = request.body.productId;

try{

	if(!userData.isAdmin){
	
	let cart = await Carts.findOne({userAccount: userData.email});
	let itemIndex = cart.orders.findIndex(product => product.productId == productId);
	if(itemIndex > -1){
		let productItem = cart.orders[itemIndex];
		cart.total -= productItem.quantity*productItem.price;
		cart.orders.splice(itemIndex,1);
	}
	cart = await cart.save();
	return response.send(cart);
	    
	}else{
		return response.send(`You don't have access in this route!`)
	}

}
catch(error){return response.send(`${userData.email} currently have no CART, go to addtoCart first!`)}
    
}
