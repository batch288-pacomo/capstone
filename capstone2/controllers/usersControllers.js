const Users = require("../models/usersModel.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// User Registration
module.exports.registerUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(result){
			return response.send(/*`${request.body.email} has been taken! Try logging in or use different email to signup!`*/false);
		}else{
			let newUser = new Users({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password,10),
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				birthDate: request.body.birthDate
			}) 
			newUser.save()
			.then(registered => response.send(/*`${request.body.email} is now registered`*/true))
			.catch(error => response.send(/*"check newUser.save() for error:"+error*/false));
		}
	}).catch(error => response.send(/*`Please enter information to register`*/false));
}

// User login
module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(result){
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if(isPasswordCorrect){
				return response.send(
					{auth: auth.createAccessToken(result)}
				)
			}else{
				return response.send(/*"Please check your Password!"*/false);
			}

		}else{
			return response.send(/*`${request.body.email} is not yet registered!`*/false)
		}
	}).catch(error => response.send(/*error*/false));
}

// User details (loginUser)
module.exports.userDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	
	Users.findOne({_id: userData.id})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// Get all users detail(admin)
module.exports.allUsers = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
		Users.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}else{
		return response.send(`You don't have access in this route!`)
	}
}

// Set user as admin(admin)
module.exports.setAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const userId = request.params.userId;
	let userAdmin = {
		isAdmin: request.body.isAdmin
	}
	if(userData.isAdmin){
		if(userAdmin.isAdmin){
			Users.findByIdAndUpdate(userId, userAdmin)
			.then(result => response.send(`account: ${result.email} is now  admin`))
			.catch(error => response.send(error));
		}else{
			Users.findByIdAndUpdate(userId, userAdmin)
			.then(result => response.send(`account: ${result.email} is now non-admin`))
			.catch(error => response.send(error));
		}
	}else{
		return response.send(`You don't have access in this route!`)
	}
}
// ///////////////////////////////////////////////
module.exports.retrieveUserDetails = (request,response) => {
	const userData = auth.decode(request.headers.authorization);
	Users.findOne({_id: userData.id})
	.then(data => response.send(data))
	.catch(error => {
		return response.send(error);
	})
}