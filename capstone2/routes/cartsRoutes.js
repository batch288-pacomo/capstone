const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const cartsControllers = require("../controllers/cartsControllers.js");


// Cart HERE
// check cart(login User)
router.get("/getProduct", auth.verify, cartsControllers.getProduct);
// add to cart(login User)
router.post("/addtoCart", auth.verify, cartsControllers.addtoCart);
// delete to cart(login User)
router.delete("/removetoCart", auth.verify, cartsControllers.removetoCart);






module.exports = router;