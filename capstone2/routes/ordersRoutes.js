const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const ordersControllers = require("../controllers/ordersControllers.js");

// Check my Order(non-admin,token)
router.get("/myOrder", auth.verify, ordersControllers.myOrder);
// Create Order(non-admin,token)
router.post("/createOrder", auth.verify, ordersControllers.createOrder);
// Retrieve all orders(admin)
router.get("/allOrders", auth.verify, ordersControllers.allUsersOrders);



// Testing ///////////////////////////


module.exports = router;