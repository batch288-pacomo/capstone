const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const usersControllers = require("../controllers/usersControllers.js");

// User Registration
router.post("/register", usersControllers.registerUser);
// User login
router./*get*/post("/login", usersControllers.loginUser);
// User details (loginUser)
router.get("/userDetails", auth.verify, usersControllers./*userDetails*/retrieveUserDetails);
// Set user as admin(admin)
router.patch("/setAdmin/:userId", auth.verify, usersControllers.setAdmin);
// Get all users detail(admin)
router.get("/allUsers", auth.verify, usersControllers.allUsers);



module.exports = router;