const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); //to connect front/backend

const usersRoutes = require("./routes/usersRoutes.js")
const productsRoutes = require("./routes/productsRoutes.js");
const ordersRoutes = require("./routes/ordersRoutes.js")
const cartsRoutes = require("./routes/cartsRoutes.js")

const port = 4004;
const app = express();
mongoose.connect("mongodb+srv://admin:admin@batch288pacomo.ccdsv9l.mongodb.net/E-commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// MongoDB connections
const db = mongoose.connection;
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));
	db.once("open", () => console.log("We are now connected to the db!"));

// Middlewawres
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())

// Routes
app.use("/users", usersRoutes);
app.use("/products", productsRoutes);
app.use("/orders", ordersRoutes);
app.use("/carts", cartsRoutes);



// app listen
app.listen(port, () => console.log(`The server is running at port ${port}!`));