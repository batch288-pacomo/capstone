import UserContext from '../UserContext.js';
import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Link, useNavigate, Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import Swal2 from 'sweetalert2';

export default function Register(){

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [birthDate, setBirthDate] = useState('');
	
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	const [isDisabled, setIsDisabled] = useState(true);
	console.log(user);
	console.log(user.id);
	useEffect(()=>{
		if (email !== '' && password1 !=='' && password2 !== '' && password1 === password2 && password1.length > 6){
			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}
	}, [email, password1, password2]);

	function register(event){
		event.preventDefault()

		fetch(`http://localhost:4004/users/register`,{
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password:password1,
				birthDate: birthDate
			})
		})

		.then(result => result.json())
		.then(data => {
		if(data === false){
				Swal2.fire({
				    title: 'Registration failed!',
				    icon: 'error',
				    text: 'Check your details/email and try again!'
				})
			}else{
				localStorage.setItem('firstName', firstName);
				setUser(localStorage.getItem('firstName'));
				localStorage.setItem('lastName', lastName);
				setUser(localStorage.getItem('lastName'));
				localStorage.setItem('email', email);
				setUser(localStorage.getItem('email'));
				localStorage.setItem('password', password1);
				setUser(localStorage.getItem('password'));
				localStorage.setItem('birthDate', birthDate);
				setUser(localStorage.getItem('birthDate'));
				Swal2.fire({
				    title : 'Registration Successful',
				    icon : 'success',
				    text: 'Log to your account!'
				    })
				navigate('/login');

			}
		})
	}


	return(

		<Container className='my-5'>
		<Row>
		<Col className='mx-auto col-6'>
			<Form onSubmit = {event => register(event)}>

			<h2 className='text-center'>Create New Account</h2>
			<h4 className='my-3'>Fill Personal Information</h4>
		      <Form.Group className="mb-3" controlId="first-name">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control
		         type="text" 
		         value={firstName}
		         onChange = {event => {
		         	setFirstName(event.target.value)
		         }}
		         placeholder="Enter your name"/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="last-name">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        type="text" 
		        value={lastName}
		        onChange = {event => {
		        	setLastName(event.target.value)
		        }}
		        placeholder="Enter your name"/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="email">
		        <Form.Label>Email</Form.Label>
		        <Form.Control 
		        type="email" 
		        value = {email}
		        onChange = {event => {
		        	console.log(event)
		        	setEmail(event.target.value)
		        }}
		        placeholder="Enter your Email"/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="birth-date">
		        <Form.Label>Birth Date</Form.Label>
		        <Form.Control 
		        type="text" 
		        value = {birthDate}
		        onChange = {event => {
		        	console.log(event)
		        	setBirthDate(event.target.value)
		        }}
		        placeholder="MM/DD/YY" />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        type="password" 
		        value = {password1}
		        onChange = {event => setPassword1(event.target.value)}
		        placeholder="Enter your password"/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="confirm-password">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control 
		        type="password" 
		        value = {password2}
		        onChange = {event => setPassword2(event.target.value)}
		        placeholder="Retype your password"/>
		      </Form.Group>

		      <p>Have an account already? <Link to = '/login'>Log in here</Link></p>


		      <Button
		      variant="primary" 
		      type="submit"
		      disabled={isDisabled}>REGISTER</Button>

		    </Form>

		</Col>
		</Row>
		</Container>

		)
}

	