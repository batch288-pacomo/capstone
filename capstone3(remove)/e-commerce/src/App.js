import './App.css';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';
import 'bootstrap/dist/css/bootstrap.min.css';

// Pages
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import PageNotFound from './pages/PageNotFound.js';
import ProductCatalog from './pages/ProductCatalog.js';
import AdminDashboard from './pages/AdminDashboard.js';
import CheckOut from './pages/CheckOut.js';

// components
import AppNavBar from './components/AppNavBar.js';
import CreateProduct from './components/CreateProduct.js';
import UpdateProduct from './components/UpdateProduct.js';

import {BrowserRouter, Route, Routes} from 'react-router-dom';


function App() {

  const[user, setUser] = useState({
    id:null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
  
/*  useEffect(()=> {
    console.log(user.isAdmin);
    console.log(localStorage.getItem('token'));

  }, [user])*/
  
  useEffect(()=>{
    if(localStorage.getItem('token')){
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          setUser({
              id : data._id,
              isAdmin: data.isAdmin
          });

      })
    }
  }, [])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
          <AppNavBar/>
        <Routes>
          <Route path = '/' element = {<Home/>} />
          {/*<Route path = '/update-product' element = {<UpdateProduct/>} />*/}
          <Route path = '/products/:productId' element = {<UpdateProduct/>} />
          <Route path = '/create-product' element = {<CreateProduct/>} />
          <Route path = '/login' element = {localStorage.token ? <PageNotFound/> : <Login/>} />
          <Route path = '/logout' element = {<Logout/>} />
          <Route path = 'register' element = {localStorage.token ? <PageNotFound/> : <Register/>} />
          <Route path = 'product-catalog' element = {<ProductCatalog/>} />
          <Route path = 'admin-dashboard' element = {<AdminDashboard/>} />
          <Route path = 'checkout-product' element = {localStorage.token ? <CheckOut/> : <Login/> } />
          <Route path = '*' element = {<PageNotFound/>} />
        </Routes>
      </BrowserRouter>
     </UserProvider>
  );
}

export default App;
