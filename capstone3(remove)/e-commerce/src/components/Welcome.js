import {Container, Row, Col} from 'react-bootstrap';

export default function Welcome(){

	return(
		<Container className='mt-5'>
			<Row>
				<Col className='mx-auto col-12'>
					<h1>Welcome to <strong>nandemoAnything</strong> e-commerce website</h1>
				</Col>
			</Row>
		</Container>


		)
}