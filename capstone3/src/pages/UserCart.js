import {useParams, useNavigate} from 'react-router-dom';
import {Container, Row, Col, Button, Card, Form, InputGroup} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext.js';
import {useState, useEffect, useContext} from 'react';



export default function UserCart(){
	const {productId} = useParams();
	const [pId, setpId] = useState('');	
	const [quantity, setQuantity] = useState(0);
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');	

	const [userAccount, setUserAccount] = useState('');
	const [total, setTotal] = useState(0);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			console.log(data.productId);
			setpId(data.productId);
			setProductName(data.productName);
			setPrice(data.price);
			setDescription(data.description);

		})
	}, [pId, productName, quantity, userAccount])

function addtoCart(){

event.preventDefault();
	if(quantity == 0){
		Swal2.fire({
			title: 'Add to Cart failed',
			icon: 'error',
			text: "Check your product quantity."
		})
	}else{
		console.log(pId);
		fetch(`${process.env.REACT_APP_API_URL}/carts/addtoCart`,{
			method: 'POST',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				productId: pId,
				quantity: parseFloat(quantity)
			})
		})

		Swal2.fire({
			title: 'Add to Cart',
			icon: 'success',
			text: `${productName} successfully added to cart!`
		})
		setTimeout(() => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/getProduct`,{
			method: 'GET',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setUserAccount(data.userAccount);
			setTotal(data.total);
		})
		}, 200);
	}
}

function removeCart(){

event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/carts/removetoCart`,{
			method: 'DELETE',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				productId: pId,
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setUserAccount(data.userAccount);
			setTotal(data.total);
		})

		Swal2.fire({
			title: 'Remove to Cart',
			icon: 'success',
			text: `${productName} successfully remove`
		})
}

	return(
		<>
		<Container className = "mt-5 col-12">
		<Row>
			<Col className = "col-12">
			<Card className = "cardHighlight">
			     <Card.Body>
			       <Card.Title>{productName}</Card.Title>
			       <Card.Subtitle>Description:</Card.Subtitle>
			       <Card.Text>{description}</Card.Text>
			       <Card.Subtitle>Price:</Card.Subtitle>
			       <Card.Text>{price}</Card.Text>
			       <Card.Subtitle>productId:</Card.Subtitle>
			       <Card.Text>{pId}</Card.Text> 
			     </Card.Body>
			</Card>

			<form className = "mt-3 col-6" onSubmit = {event => addtoCart(event)}>
			      <InputGroup className="mb-1">
			        <InputGroup.Text>Quantity</InputGroup.Text>
				    <Form.Control
				          placeholder="ex. no"
				          type="number" 
				          value={quantity}
				          onChange = {event => {
				          	setQuantity(event.target.value)
				          }}
				        />
				  </InputGroup>

			      <Button
			      	className = "mt-0"
				      variant="primary" 
				      type="submit"
				      onClick = {() => addtoCart(pId)}
				      >Add to Cart
				  </Button>

				  <Button
			      	className = "mt-0 ms-3"
				      variant="primary" 
				      type="submit"
				      onClick = {() => removeCart(pId)}
				      >Remove to Cart
				  </Button>

			</form>   
			</Col>

	<h3 className = 'my-3'>Basket</h3>

			<Col className = "col-12 mt-5">
			<Card className = "cardHighlight">
			     <Card.Body>
			       <Card.Subtitle>Account:</Card.Subtitle>
			       <Card.Text>{userAccount}</Card.Text>
			       <Card.Subtitle>Total Amount:</Card.Subtitle>
			       <Card.Text>{total}</Card.Text>	       
			     </Card.Body>
			</Card>

			<form className = "mt-3 col-6" 
			onSubmit = {event => addtoCart(event)}
			onSubmit = {event => removeCart(event)}>   
			</form>   
			</Col>

		</Row>
		</Container>
		</>
		)


}


	

