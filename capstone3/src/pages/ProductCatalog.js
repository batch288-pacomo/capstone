import ProductList from '../components/ProductList.js';
import {Container, Row, Col, Form, InputGroup} from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function ProductCatalog(){

	const [products, setProducts] = useState([]);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(result => result.json())
		.then(data => {
			 setProducts(data.map(product =>{
			 	return(
			 		<ProductList key = {product._id} productProp = {product}/>
			 		)
			 }))

		})
	}
	, [])



	return(
		<>
		<h1 className = 'text-center my-3'>Product Catalog</h1>
		{products}
		<h1 className = 'text-center my-3'></h1>
		</>
		)
}