import CreateProduct from '../components/CreateProduct.js';
import RetrieveAllProduct from '../components/RetrieveAllProduct.js';

export default function AdminDashboard(){

	return(
		<>
		<CreateProduct/>
		<RetrieveAllProduct/>
		</>
		)
}