import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import '../Home.css'; // Import your custom CSS file
import {Link} from 'react-router-dom';


export default function Home() {
  return (
    <Container className="col-12">
      <Row>
        <Col>
          <div className="welcome-container">
            <h1 className="store-name">Nandemo Anything</h1>
            <p className="description">
              Welcome to <span className="light-text">Nandemo Anything</span>, your ultimate destination for all things extraordinary and unexpected! We believe that life is too short to be limited by ordinary choices, which is why we're here to inspire your imagination and bring your wildest dreams to life. Whether you're searching for unique gifts, quirky home decor, or out-of-this-world gadgets, we've got you covered. So come on in and embark on a thrilling journey of endless possibilities!
            </p>
            <Button  as = {Link} to = '/product-catalog'
            className="start-shopping-button">Start Shopping</Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
}