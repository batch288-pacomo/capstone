import React from 'react';

// Create a Context Object - a data that can be used to store information that can be shared to other components within the app
// Context Object - is a different approach to passing information between components and allows easier access by avoiding the use of prop passing

// With the help of createContext() method we were able to create a context stored in variable UserContext
const ProductContext = React.createContext();

// The 'Provider' component allows other components to consume/ use the context object and supply necessary information needed to the context object
export const ProductProvider = ProductContext.Provider;

export default ProductContext;