import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import {Container, Row, Col, Button, Card, Form, InputGroup} from 'react-bootstrap';
import Swal2 from 'sweetalert2';


export default function UpdateProduct(){


	const [productName, setProductName] = useState('');
	const [id, setId] = useState('');
	const [archive, setArchive] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const navigate = useNavigate();
	const {productId} = useParams();
	console.log(productId);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			// setProductId(data.productId;
			setProductName(data.productName);
			setId(data.productId);
			setArchive(data.isActive);
			setDescription(data.description);
			setPrice(data.price);
		console.log(productId)
		console.log(data);
		})
	}, [])

	const updateProduct = () => {
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${productId}`,{
			method: 'PATCH',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				productName: productName,
				productId: id,
				isActive: archive,
				price: price,
				description: description 
			})
		})
				Swal2.fire({
					title: 'Product Updated',
					icon: 'success',
					text: "Successfully updated product!"
				})
				navigate('/admin-dashboard');				
	}

return(
	<>
	
	 	<Container className='my-5'>
	 		<Row>
			<h1 className = 'text-center my-3'>Update Product</h1>
	 			<Col className='mx-auto col-12'>
	 			<form onSubmit = {event => updateProduct(event)}>
	 			      <InputGroup className="mb-3">
	 			        <InputGroup.Text>Product Name</InputGroup.Text>
				        <Form.Control
	 			          placeholder="Title"
	 			          type="text" 
	 			          value={productName}
	 			          onChange = {event => {
	 			          	setProductName(event.target.value)
	 			          }}
	 			        />
				      </InputGroup>

				      <InputGroup className="mb-3">
			      		<InputGroup.Text>Product Id</InputGroup.Text>
				        <Form.Control
				          placeholder="ex. NS000"
	 			          type="text" 
				          value={id}
 			          onChange = {event => {
				          	setId(event.target.value)
	 			          }}
				        /> 
	 			      </InputGroup>

	 			      <InputGroup className="mb-3">
			      		<InputGroup.Text>Active/Archive</InputGroup.Text>
				        <Form.Control
				          placeholder="Active = true, Archive = false"
	 			          type="text" 

 			          onChange = {event => {
				          	setArchive(event.target.value)
	 			          }}
				        /> 
	 			      </InputGroup>

	 			      <InputGroup className="mb-3">
	 			        <InputGroup.Text>Price</InputGroup.Text>
				        <Form.Control 
				        placeholder="enter price"
	 			        type="number" 
	 			        value={price}
	 			        onChange = {event => {
			        	setPrice(event.target.value)
	 			        }}
				        />
	 			      </InputGroup>

	 			      <InputGroup>
	 			        <InputGroup.Text>Description</InputGroup.Text>
				        <Form.Control 
	 			        as="textarea"
	 			        placeholder="product description"
	 			        type="text" 
	 			        value={description}
	 			        onChange = {event => {
	 			        	setDescription(event.target.value)
	 			        }}
	 			        />
	 			      </InputGroup>

	 			      <Button
	 			      	className = "my-3"
	 				      variant="primary" 
	 				      type="submit"
	 				      onClick = {() => updateProduct(productId)}
	 				      >UPDATE PRODUCT
	 				  </Button>

	 			</form>
	 			</Col>
	 		</Row>
	 	</Container>
	</>
	)

}


