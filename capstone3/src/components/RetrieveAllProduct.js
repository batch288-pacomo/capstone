
import ProductList from './ProductList.js';
import {Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function RetrieveAllProduct(){

	const [products, setProducts] = useState([]);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
		.then(result => result.json())
		.then(data => {

			 setProducts(data.map(product =>{
			 	console.log(product);
			 	return(
			 		<ProductList key = {product._id} productProp = {product}/>
			 		)
			 }))

		})
	}
	, [])

	return(
		<>
		<h1 className = 'text-center my-3'>Retrive All product</h1>
		{products}
		</>

		)
}