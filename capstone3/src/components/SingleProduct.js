import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import {Container, Row, Col, Button, Card, Form, InputGroup} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function SingleProduct(){
	
	const [productName, setProductName] = useState('');
	const [id, setId] = useState('');
	const [archive, setArchive] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const navigate = useNavigate();
	const {productId} = useParams();
	const [quantity, setQuantity] = useState(0);
	const [latestOrder, setLatestOrder] = useState('');

	const [cTotal, setcTotal] = useState('');
	const [csubTotal, setcSubTotal] = useState('');
	const [cAccount, setcAccount] = useState('')

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {

			setProductName(data.productName);
			setId(data.productId);
			setArchive(data.isActive);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [])

	function createOrder(){
		event.preventDefault();

	if(quantity == 0){
		Swal2.fire({
			title: 'Order failed',
			icon: 'error',
			text: "Check your product quantity."
		})
		setcTotal(undefined)
		setcSubTotal(undefined)
	}else{
		fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`,{
			method: 'POST',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				productId: id,
				quantity: parseFloat(quantity)
			})
		})
		setTimeout(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/myOrder`,{
			method: 'GET',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			}
		})
		.then(result => result.json())
		.then(data => {

			const latestOrder = data.slice(-1)[0];
			setLatestOrder(latestOrder);
			setcTotal(latestOrder.orders[0].price*latestOrder.orders[0].quantity)
			setcSubTotal(latestOrder.orders[0].price)
			setcAccount(latestOrder.userAccount);
		})
			Swal2.fire({
			title: 'Order Confirmation',
			icon: 'success',
			text: `Thank you for placing your order with us! Your ${productName} order has been successfully sent.`
		})
			}, 200);
	}

		


	}

	return(
		<>
		<Container className = "mt-5 col-12">
		<Row>
			<Col className = "col-12">
			<Card className = "cardHighlight">
			     <Card.Body>
			       <Card.Title>{productName}</Card.Title>
			       <Card.Subtitle>Description:</Card.Subtitle>
			       <Card.Text>{description}</Card.Text>
			       <Card.Subtitle>Price:</Card.Subtitle>
			       <Card.Text>{price}</Card.Text>
			       <Card.Subtitle>productId:</Card.Subtitle>
			       <Card.Text>{id}</Card.Text> 
			     </Card.Body>
			</Card>

			<form className = "mt-3 col-6" onSubmit = {event => createOrder(event)}>
			      <InputGroup className="mb-1">
			        <InputGroup.Text>Quantity</InputGroup.Text>
				    <Form.Control
				          placeholder="ex. no"
				          type="number" 
				          value={quantity}
				          onChange = {event => {
				          	setQuantity(event.target.value)
				          }}
				        />
				  </InputGroup>

			      <Button
			      	className = "mt-0"
				      variant="primary" 
				      type="submit"
				      onClick = {() => createOrder(id)}
				      >Check Out
				  </Button>

			</form>   
			</Col>

	<h3 className = 'my-3'>Basket</h3>

			<Col className = "col-12 my-5">
			<Card className = "cardHighlight">
			     <Card.Body>
			       <Card.Subtitle>Account:</Card.Subtitle>
			       <Card.Text>{cAccount}</Card.Text>
			       <Card.Subtitle>Price:</Card.Subtitle>
			       <Card.Text>{csubTotal}</Card.Text>
			       <Card.Subtitle>Total Amount:</Card.Subtitle>
			       <Card.Text>{cTotal}</Card.Text>	       
			     </Card.Body>
			</Card>

			</Col>

		</Row>
		</Container>
		</>
		)
}

