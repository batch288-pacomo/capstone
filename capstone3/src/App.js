import './App.css';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';

// pages
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import PageNotFound from './pages/PageNotFound.js';
import Login from './pages/Login.js';
import AdminDashboard from './pages/AdminDashboard.js';
import ProductCatalog from './pages/ProductCatalog.js';
import UserCart from './pages/UserCart.js';
import UserOrders from './pages/UserOrders.js';

// components
import AppNavBar from './components/AppNavBar.js';
import Logout from './components/Logout.js';
import UpdateProduct from './components/UpdateProduct.js';
import SingleProduct from './components/SingleProduct.js'


function App() {

  const[user, setUser] = useState({
    id:null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    if(localStorage.getItem('token')){
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          setUser({
              id : data._id,
              isAdmin: data.isAdmin
          });

      })
    }
  }, [])

  return (
    
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
          <AppNavBar/>
        <Routes>
          <Route path = '/' element = {<Home/>} />

          <Route path = 'register' element = {localStorage.token ? <PageNotFound/> : <Register/>} />

          <Route path = '/login' element = {localStorage.token ? <PageNotFound/> : <Login/>} />

          <Route path = '/logout' element = {<Logout/>} />

          <Route path = 'checkout-product' element = {/*localStorage.token ? <CheckOut/> : */<Login/> } />

          <Route path = 'admin-dashboard' element = {<AdminDashboard/>} />

          <Route path = 'product-catalog' element = {<ProductCatalog/>} />

          <Route path = '*' element = {<PageNotFound/>} />

          <Route path = 'user-cart/:productId' element = {<UserCart/>} />

          <Route path = '/products/:productId' element = {<UpdateProduct/>} />

          <Route path = '/single-products/:productId' element = {<SingleProduct/>} />

          <Route path = 'user-orders' element = {<UserOrders/>} />

        </Routes>
      </BrowserRouter>
     </UserProvider>

  );

}

export default App;
